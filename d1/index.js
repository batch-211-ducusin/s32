//Node Routing with HTTP Methods

/*
The endpoint of our request URL can be found as the url property of the request object. (request.url)

if(request.url == '/greeting'){
	
	response.writeHead(200, {'content-type': 'text/plain'});
	response.end('Hello World');

} else if(request.url == '/homepage'){
	
	response.

}

This is sufficient for retrieving resources from different


HTTP Methods
	GET - retrieves resources
	POST - sends data for creating a resource
	PUT - sends data for updating a resource
	DELETE - deletes a specified resource


Postman Client
	We'll use "Postman" client to improve the experience of sending requests, inspecting responses, and debugging.
*/



//Code-along
/*
NODE.JS ROUTING WITH HTTP METHODS
*/
let http = require('http');
http.createServer(function (request, response) {
	
	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Data retrieved from the database');
	}

	if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200, {'content-type': 'text/plain'});
		response.end('Data to be sent to the database');
	}

}).listen(4000);
console.log(`server running at localhost:4000`);